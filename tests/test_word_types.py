from zwrbl import find_wordtype


def test_adjective():
    assert find_wordtype("Schön") == "Adjektiv"


def test_word():
    assert find_wordtype("Wort") == "Substantiv"
