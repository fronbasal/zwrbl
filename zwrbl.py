import re

import requests
from lxml import html


def find_adjectives(sentence):
    """Returns an array with the adjectives in the sentence"""
    words = [re.sub(r"\W", "", word).lower() for word in sentence.split(" ")]
    adjectives = []
    for word in words:
        wordtype = find_wordtype(word)
        out = 'The wordtype of {} is: {}'.format(word, wordtype)
        if wordtype == "Adjektiv":
            adjectives.append(word)
    return adjectives


def find_wordtype(word):
    """Scrapes duden for the word type"""
    cleaned_word = word.replace("ö", "oe").replace("ä", "ae").replace("ü", "ue")
    search_page = requests.get("https://www.duden.de/suchen/dudenonline/" + cleaned_word)
    search_dom = html.fromstring(search_page.content)
    result_page = requests.get(search_dom.xpath("//main/section[2]/section/h2/a/@href")[0])
    result_dom = html.fromstring(result_page.content)
    wordtype = result_dom.xpath('//section/div/div/strong/text()')[0]
    return wordtype.split(",")[0]


def remove_plural(word):
    """Brings $word in a form that the search function can interpret"""
    return [word[-4:] if word[-4:] == "er" else word[:-4]]
