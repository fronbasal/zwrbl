FROM python:3

ADD . /
WORKDIR /

RUN pip3 install -r requirements.txt --quiet

CMD [ "python3", "-m", "pytest", "tests" ]
